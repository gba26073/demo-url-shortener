package com.example.demo.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.example.demo.model.Mapping;
import com.example.demo.service.MappingService;

import reactor.core.publisher.Mono;

@WebFluxTest
class MappingControllerTest {

	@Autowired
	private ApplicationContext context;

	@MockBean
	private MappingService service;

	private WebTestClient client;

	@BeforeEach
	public void setup() {
		client = WebTestClient.bindToApplicationContext(context).build();
	}

	@Test
	void testFindRandomUrlNotSuccessful() {
		var id = 1L;
		// @formatter:off
		when(service.find(anyLong()))
			.thenReturn(Mono.empty());
		client.get().uri("/" + id)
			.exchange()
			.expectStatus().isNotFound();
		// @formatter:on
	}

	@Test
	void testFindRandomUrl() {
		var id = 1L;
		var url = "https://google.com";
		// @formatter:off
		when(service.find(anyLong()))
			.thenReturn(Mono.just(Mapping.builder()
					.shortUrl(id)
					.originUrl(url)
					.build()));
		client.get().uri("/" + id)
			.exchange()
			.expectStatus().isFound();
		// @formatter:on
	}

	@Test
	void testShortenApi() {
		var url = "https://google.com";
		// @formatter:off
		var mapping = Mapping.builder()
			.shortUrl(1L)
			.originUrl(url)
			.build();
		when(service.shorten(any()))
			.thenReturn(Mono.just(mapping));
		client.post().uri("/api/v1/short-url")
			.contentType(MediaType.APPLICATION_JSON)
			.bodyValue(Mapping.builder().originUrl(url).build())
			.exchange()
			.expectStatus().isOk()
			.expectHeader().contentType(MediaType.APPLICATION_JSON)
			.expectBody(Mapping.class)
			.isEqualTo(mapping);
		// @formatter:on
	}

	@Test
	void testBatchShortenApi() {
		// @formatter:off
		var id = new AtomicLong(1L);
		when(service.shorten(any()))
			.then(invocation -> { 
				var mapping = (Mapping) invocation.getArgument(0);
				mapping.setShortUrl(id.getAndIncrement());
				return Mono.just(mapping);
			});
		var mappings = IntStream.range(0, 1000)
				.mapToObj(i -> Mapping.builder()
						.shortUrl((long) i + 1)
						.originUrl(getRandomUrl())
						.build())
				.collect(Collectors.toList());
		client.post().uri("/api/v1/short-urls")
			.contentType(MediaType.APPLICATION_JSON)
			.bodyValue(mappings)
			.exchange()
			.expectStatus().isOk()
			.expectHeader().contentType(MediaType.APPLICATION_JSON)
			.expectBody(new ParameterizedTypeReference<List<Mapping>>(){})
			.isEqualTo(mappings);
		// @formatter:on
	}

	private String getRandomUrl() {
		var random = new Random();
		return String.format("https://example.com?random=%08d", random.nextInt(Integer.MAX_VALUE));
	}

}
