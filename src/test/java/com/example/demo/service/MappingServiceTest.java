package com.example.demo.service;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.demo.repository.MappingEntity;
import com.example.demo.repository.MappingRepository;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
class MappingServiceTest {

	@MockBean
	private MappingRepository repository;

	@Autowired
	private MappingService service;

	@Test
	void testFindRandomUrl() {
		var shortUrl = 1L;
		var originUrl = "https://google.com";
		// 模擬底層資料庫查詢到的情況
		when(repository.findById(anyLong()))
			.thenReturn(Mono.just(MappingEntity.builder()
					.shortUrl(shortUrl)
					.originUrl(originUrl)
					.createdTime(Calendar.getInstance().getTimeInMillis())
					.build()));
		StepVerifier.create(service.find(shortUrl))
			.expectNextMatches(entity -> shortUrl == entity.getShortUrl().longValue() && originUrl.equals(entity.getOriginUrl()))
			.expectComplete()
			.verify();
	}

	@Test
	void testNotFindRandomUrl() {
		var shortUrl = 1L;
		// 模擬底層資料庫查詢不到的情況
		when(repository.findById(anyLong())).thenReturn(Mono.empty());
		StepVerifier.create(service.find(shortUrl))
			.expectComplete()
			.verify();
	}

}
