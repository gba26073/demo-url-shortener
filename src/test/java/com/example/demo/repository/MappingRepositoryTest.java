package com.example.demo.repository;

import java.util.Calendar;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import reactor.test.StepVerifier;

@SpringBootTest
class MappingRepositoryTest {

	@Autowired
	private MappingRepository repository;

	/**
	 * 如果產生一般網址的話，應該要查出來。
	 */
	@Test
	void testSaveAndFindRandomUrl() {
		var google = MappingEntity.builder()
				.originUrl("https://www.google.com/")
				.createdTime(Calendar.getInstance().getTimeInMillis())
				.build();
		StepVerifier.create(repository.save(google)
				.map(MappingEntity::getShortUrl)
				.flatMap(repository::findById)
				.map(MappingEntity::getOriginUrl))
				.expectNextMatches(google.getOriginUrl()::equals)
				.expectComplete().verify();
	}

	/**
	 * 尋找原始網址是不是有被產製過，測試一下客製化 SQL。
	 */
	@Test
	void testFindByOriginUrl() {
		var entity = MappingEntity.builder()
				.originUrl("https://www.google.com/")
				.createdTime(Calendar.getInstance().getTimeInMillis())
				.build();
		StepVerifier.create(repository.save(entity)
				.then(repository.findByOriginUrl(entity.getOriginUrl()).next()))
			.expectNextMatches(entity::equals)
			.expectComplete()
			.verify();
	}

}
