package com.example.demo.model;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 長短網址的對應關係。
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mapping {

	/**
	 * 編碼過長度較短的網址。
	 */
	@Schema(description = "產生的短網址。", accessMode = AccessMode.READ_ONLY)
	private Long shortUrl;

	/**
	 * 短網址所對應的實際網址。
	 */
	@Schema(description = "要縮短的原始網址。", example = "https://www.google.com/", required = true)
	private String originUrl;

}
