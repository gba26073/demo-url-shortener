package com.example.demo.service;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Mapping;
import com.example.demo.repository.MappingRepository;
import com.example.demo.repository.MappingEntity;

import lombok.Setter;
import reactor.core.publisher.Mono;

@Service
public class MappingService {

	/**
	 * JPA 的 DB 存取服務。
	 */
	@Setter(onMethod = @__({ @Autowired }))
	private MappingRepository repository;

	/**
	 * 根據短網址尋找原始網址。
	 * 
	 * @param shortUrl 短網址
	 * @return 包含原始網址的網址對應資訊，originUrl 應有對應內容。
	 */
	public Mono<Mapping> find(Long shortUrl) {
		return repository.findById(shortUrl)
				.log()
				// 將存取到的網址對應關係轉換成對外物件。
				.map(this::parseEntityToDto);
	}

	/**
	 * 根據原始網址產生對應短網址。
	 *
	 * @param mapping 網址對應資訊
	 * @return 包含原始網址的網址對應資訊，shortUrl 應有對應內容。
	 */
	public Mono<Mapping> shorten(Mapping mapping) {
		// 產生新的短網址
		var createMapping = Mono.just(MappingEntity.builder()
				.originUrl(mapping.getOriginUrl())
				.createdTime(Calendar.getInstance().getTimeInMillis())
				.build())
				.log()
				// 將產生的網址對應存進資料庫
				.flatMap(repository::save)
				.log();
		// 先找看看原本的網址有沒有轉換過，有的話就不重新產生
		return repository.findByOriginUrl(mapping.getOriginUrl())
				.next()
				.log()
				.switchIfEmpty(createMapping)
				// 將存取到的網址對應關係轉換成對外物件。
				.map(this::parseEntityToDto)
				.log();
	}

	/**
	 * 將存取到的網址對應關係轉換成對外物件。
	 * 
	 * @param entity 短網址對應儲存類
	 * @return 對外的網址對應物件
	 */
	private Mapping parseEntityToDto(MappingEntity entity) {
		return Mapping.builder()
				.originUrl(entity.getOriginUrl())
				.shortUrl(entity.getShortUrl())
				.build();
	}

}
