package com.example.demo.config;

import java.io.IOException;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.util.StreamUtils;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class DbInitializer {

	private static final String DEFAULT_FILENAME = "schema.sql";

	@Setter(onMethod = @__({ @Autowired }))
	private DatabaseClient client;

	/**
	 * 執行初始化 SQL
	 *
	 * @throws IOException 讀取檔案有問題
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void initDatabaseAfterStartup() throws IOException {
		var schemaSql = new ClassPathResource(DEFAULT_FILENAME).getInputStream();
		var schema = StreamUtils.copyToString(schemaSql, Charset.defaultCharset());
		client.sql(schema)
				.fetch()
				.rowsUpdated()
				.doOnNext(rowsUpdated -> log.debug("execute {} -> {}", DEFAULT_FILENAME, rowsUpdated))
				.block();
	}

}
