package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Mapping;
import com.example.demo.service.MappingService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Setter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@Tag(name = "URL Shortener")
public class MappingController {

	@Setter(onMethod = @__({ @Autowired }))
	private MappingService service;

	@Operation(summary = "短網址重導向原網址。")
	@GetMapping("/{shortUrl}")
	public Mono<Void> redirect(ServerHttpResponse response, @PathVariable Long shortUrl) {
		response.setStatusCode(HttpStatus.NOT_FOUND);
		return service.find(shortUrl)
				.map(mapping -> URI.create(mapping.getOriginUrl()))
				.doOnNext(uri -> {
					response.getHeaders().setLocation(uri);
					response.setStatusCode(HttpStatus.FOUND);
				})
				.then(response.setComplete());
	}

	@Operation(summary = "原網址縮短成短網址。")
	@PostMapping("/api/v1/short-url")
	public Mono<Mapping> shorten(@RequestBody Mapping mapping) {
		return service.shorten(mapping).log();
	}

	@Operation(summary = "原網址批次縮短成短網址。")
	@PostMapping("/api/v1/short-urls")
	public Flux<Mapping> shorten(@RequestBody List<Mapping> mappings) {
		return Flux.fromIterable(mappings)
				.flatMap(service::shorten);
	}

}
