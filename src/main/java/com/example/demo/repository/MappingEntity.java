package com.example.demo.repository;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
public class MappingEntity {

	@Id
	private Long shortUrl;

	/**
	 * 短網址所對應的實際網址。
	 */
	private String originUrl;

	/**
	 * 短網址產生時間。
	 */
	private long createdTime;

}
