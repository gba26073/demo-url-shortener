package com.example.demo.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;

@Repository
public interface MappingRepository extends ReactiveCrudRepository<MappingEntity, Long> {

	@Query("SELECT * FROM mapping_entity WHERE origin_url = :originUrl")
	Flux<MappingEntity> findByOriginUrl(String originUrl);

}
