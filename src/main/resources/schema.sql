CREATE TABLE IF NOT EXISTS mapping_entity (
    short_url BIGSERIAL PRIMARY KEY,
    origin_url VARCHAR(1023),
    created_time BIGINT
);